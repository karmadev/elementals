package es.karmadev.elementals.injection;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import es.karmadev.elementals.ElementalsPlugin;

public class ElementalBinderModule extends AbstractModule {

    private final ElementalsPlugin plugin;

    public ElementalBinderModule(final ElementalsPlugin plugin) {
        this.plugin = plugin;
    }

    public Injector createInjector() {
        return Guice.createInjector(this);
    }

    @Override
    public void configure() {
        this.bind(ElementalsPlugin.class).toInstance(this.plugin);
    }
}
