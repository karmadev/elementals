package es.karmadev.elementals.element;

import com.google.inject.Inject;
import es.karmadev.elementals.ElementalsPlugin;
import es.karmadev.elementals.api.annotations.immutability.Mutable;
import es.karmadev.elementals.api.element.Element;
import es.karmadev.elementals.api.element.ElementFactory;
import es.karmadev.elementals.api.element.property.ElementProperty;
import es.karmadev.elementals.api.exception.immutability.ImmutableViolationException;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

public class PluginElementFactory implements ElementFactory {

    @Inject
    private ElementalsPlugin plugin;
    private final Set<Element> registered = ConcurrentHashMap.newKeySet();

    /**
     * Create a new element
     *
     * @param name        the element name
     * @param description the element description
     * @param properties  the element properties
     * @return the element
     * @throws NullPointerException if the name or description are null.
     */
    @Override
    public Element createElement(final String name, final String description, final Collection<ElementProperty> properties)
            throws NullPointerException {
        if (name == null || description == null)
            throw new NullPointerException("Name and/or description cannot be null");

        return new PluginElement(name, description, properties);
    }

    /**
     * Tries to register an element
     *
     * @param element the element to register
     * @return if the element was registered
     * @throws ImmutableViolationException if the element {@link Element#getProperties() properties} are
     *                                     not immutable
     */
    @Override
    public boolean register(final Element element) throws ImmutableViolationException {
        if (element == null) return false;
        if (isCollectionMutable(element.getProperties()))
            throw new ImmutableViolationException(element, Element.class);

        Class<? extends Element> clazz = element.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Mutable.class)) continue;

            field.setAccessible(true);
            MethodHandle getter;

            try {
                getter = getGetterHandle(field);
                Object value = getter.invoke(element);
                if (value instanceof Collection && !isCollectionMutable((Collection<?>) value))
                    throw new ImmutableViolationException(element, Element.class);
            } catch (Throwable ex) {
                plugin.getLogger().log(Level.SEVERE, ex, () -> "Failed to validate element " + element + " immutability");
                return false;
            }
        }

        return registered.add(element);
    }

    /**
     * Get if an element has been registered
     *
     * @param element the element
     * @return if the element has been
     * registered
     */
    @Override
    public boolean isRegistered(final Element element) {
        return registered.contains(element);
    }

    /**
     * Get all the registered elements
     *
     * @return the registered elements
     */
    @Override
    public Collection<Element> getRegisteredElements() {
        return Collections.unmodifiableSet(this.registered);
    }

    private static boolean isCollectionMutable(final Collection<?> collection) {
        try {
            collection.add(null); // This should throw an exception if the collection is unmodifiable
            return true;
        } catch (UnsupportedOperationException e) {
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    private static MethodHandle getGetterHandle(Field field) throws IllegalAccessException {
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        return lookup.unreflectGetter(field);
    }
}