package es.karmadev.elementals.element;

import es.karmadev.elementals.api.element.Element;
import es.karmadev.elementals.api.element.property.ElementProperty;

import java.util.Collection;
import java.util.Collections;

public class PluginElement implements Element {

    private final String name;
    private final String description;
    private final Collection<ElementProperty> properties;

    public PluginElement(final String name, final String description, final Collection<ElementProperty> properties) {
        this.name = name;
        this.description = description;
        this.properties = (properties == null ? Collections.emptyList() :
                Collections.unmodifiableCollection(properties));
    }

    /**
     * Get the element name
     *
     * @return the element name
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Get the element description
     *
     * @return the description
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /**
     * Get the element properties
     *
     * @return the element properties
     */
    @Override
    public Collection<ElementProperty> getProperties() {
        return this.properties;
    }
}
