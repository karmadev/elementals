package es.karmadev.elementals.element.property;

import es.karmadev.elementals.api.element.property.ElementProperty;
import es.karmadev.elementals.api.element.property.PropertyType;

public class PluginElementProperty implements ElementProperty {

    private final String description;
    private final PropertyType type;

    public PluginElementProperty(final String description, final PropertyType type) {
        this.description = description;
        this.type = type;
    }

    /**
     * Get the element property
     * description
     *
     * @return the property description
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /**
     * Get the element property type
     *
     * @return the property type
     */
    @Override
    public PropertyType getType() {
        return this.type;
    }
}
