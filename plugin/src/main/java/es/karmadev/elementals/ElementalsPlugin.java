package es.karmadev.elementals;

import com.google.inject.Injector;
import es.karmadev.elementals.injection.ElementalBinderModule;
import org.bukkit.plugin.java.JavaPlugin;

public class ElementalsPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        ElementalBinderModule module = new ElementalBinderModule(this);
        Injector injector = module.createInjector();
        injector.injectMembers(this);


    }
}
