package es.karmadev.elementals.client;

import es.karmadev.elementals.api.client.Elemental;
import es.karmadev.elementals.api.element.Element;

import java.util.UUID;

public class ElementalClient implements Elemental {

    public ElementalClient() {

    }

    /**
     * Get the elemental name
     *
     * @return the client name
     */
    @Override
    public String getName() {
        return "";
    }

    /**
     * Get the elemental unique ID
     *
     * @return the unique ID
     */
    @Override
    public UUID getUniqueId() {
        return null;
    }

    /**
     * Get the client element
     *
     * @return the element of the
     * client
     */
    @Override
    public Element getElement() {
        return null;
    }

    /**
     * Set the client new element
     *
     * @param newElement the client new
     *                   element
     */
    @Override
    public void setElement(Element newElement) {

    }
}
