package es.karmadev.elementals.api.element.property;

/**
 * Represents a property type
 */
public enum PropertyType implements Comparable<PropertyType> {
    /**
     * The property is a buff
     */
    BUFF,
    /**
     * The property is a de-buff
     */
    DEBUFF;
}
