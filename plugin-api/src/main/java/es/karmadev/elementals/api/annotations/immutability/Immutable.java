package es.karmadev.elementals.api.annotations.immutability;

/**
 * Represents an object with
 * immutable elements
 */
public @interface Immutable {


}
