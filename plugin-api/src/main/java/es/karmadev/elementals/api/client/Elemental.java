package es.karmadev.elementals.api.client;

import es.karmadev.elementals.api.element.Element;

import java.util.UUID;

/**
 * Represents a client in the
 * game
 */
public interface Elemental {

    /**
     * Get the elemental name
     *
     * @return the client name
     */
    String getName();

    /**
     * Get the elemental unique ID
     *
     * @return the unique ID
     */
    UUID getUniqueId();

    /**
     * Get the client element
     *
     * @return the element of the
     * client
     */
    Element getElement();

    /**
     * Set the client new element
     *
     * @param newElement the client new
     *                   element
     */
    void setElement(final Element newElement);
}