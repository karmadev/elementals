package es.karmadev.elementals.api.element;

import es.karmadev.elementals.api.annotations.immutability.Immutable;
import es.karmadev.elementals.api.element.property.ElementProperty;
import es.karmadev.elementals.api.element.property.PropertyType;

import java.util.Collection;

/**
 * Represents an element
 */
@Immutable
public interface Element {

    /**
     * Get the element name
     *
     * @return the element name
     */
    String getName();

    /**
     * Get the element description
     *
     * @return the description
     */
    String getDescription();

    /**
     * Get the element properties
     *
     * @return the element properties
     */
    Collection<ElementProperty> getProperties();

    /**
     * Build the element stats description
     *
     * @return the stats description
     */
    default String buildStatsDescription() {
        char COLOR_CHAR = '§';
        String description = this.getDescription();
        Collection<ElementProperty> properties = this.getProperties();

        if (properties.isEmpty())
            return String.format("%c7%s", COLOR_CHAR, description);

        StringBuilder builder = new StringBuilder();
        builder.append(COLOR_CHAR).append('7').append(description)
                .append('\n')
                .append(COLOR_CHAR).append('0')
                .append(COLOR_CHAR).append('m')
                .append("-------------------")
                .append('\n');

        properties.stream().sorted(ElementProperty::compareTo)
                .forEachOrdered((property) -> {
                    builder.append('\n').append(COLOR_CHAR);

                    if (property.getType().equals(PropertyType.BUFF)) {
                        builder.append("a+ ");
                    } else {
                        builder.append("c- ");
                    }

                    builder.append(property.getDescription());
                });
        return builder.toString();
    }
}