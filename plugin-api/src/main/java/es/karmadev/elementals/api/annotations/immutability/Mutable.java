package es.karmadev.elementals.api.annotations.immutability;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Provides an element which is allowed
 * to be mutable
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Mutable {
}
