package es.karmadev.elementals.api;

import es.karmadev.elementals.api.client.Elemental;
import es.karmadev.elementals.api.element.ElementFactory;

import java.util.Collection;
import java.util.UUID;

/**
 * Elementals API
 */
public interface ElementalAPI {

    /**
     * Get an elemental by its unique
     * ID
     *
     * @param uniqueId the unique id to search for
     * @return the elemental
     */
    Elemental getElemental(final UUID uniqueId);

    /**
     * Get all the elementals
     *
     * @return the elementals
     */
    Collection<Elemental> getElementals();

    /**
     * Get the element factory
     *
     * @return the element factory
     */
    ElementFactory getElementFactory();
}