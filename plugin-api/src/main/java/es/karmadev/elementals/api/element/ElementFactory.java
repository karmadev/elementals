package es.karmadev.elementals.api.element;

import es.karmadev.elementals.api.element.property.ElementProperty;
import es.karmadev.elementals.api.exception.immutability.ImmutableViolationException;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Represents an element factory
 */
public interface ElementFactory {

    /**
     * Create a new element
     *
     * @param name the element name
     * @param description the element description
     * @param properties the element properties
     * @return the element
     */
    default Element createElement(final String name, final String description, final ElementProperty... properties) {
        if (properties == null || properties.length == 0)
            return this.createElement(name, description, Collections.emptyList());

        return this.createElement(name, description, Arrays.asList(properties));
    }

    /**
     * Create a new element
     *
     * @param name the element name
     * @param description the element description
     * @param properties the element properties
     * @return the element
     */
    Element createElement(final String name, final String description, final Collection<ElementProperty> properties);

    /**
     * Tries to register an element
     *
     * @param element the element to register
     * @return if the element was registered
     * @throws ImmutableViolationException if the element {@link Element#getProperties() properties} are
     * not immutable
     */
    boolean register(final Element element) throws ImmutableViolationException;

    /**
     * Get if an element has been registered
     *
     * @param element the element
     * @return if the element has been
     * registered
     */
    boolean isRegistered(final Element element);

    /**
     * Get all the registered elements
     *
     * @return the registered elements
     */
    Collection<Element> getRegisteredElements();
}
