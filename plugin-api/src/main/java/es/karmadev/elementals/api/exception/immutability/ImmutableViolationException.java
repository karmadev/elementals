package es.karmadev.elementals.api.exception.immutability;

/**
 * ImmutableViolationException exceptions is thrown when a class
 * is annotated with {@link es.karmadev.elementals.api.annotations.immutability.Immutable} and the
 * provided class does not provide an immutable
 * object
 */
public class ImmutableViolationException extends Exception {

    /**
     * Initialize the exception
     *
     * @param object the object
     * @param type the object type
     */
    public ImmutableViolationException(final Object object, final Class<?> type) {
        super("Immutability violation exception. Cannot handle object " + object + " properly because objects of type " + type.getSimpleName() + " require an immutability level");
    }
}
